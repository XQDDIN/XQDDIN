---
layout: post
title: 博客网站搭建
image: blog.jpg
author: XQDD
date: 2020-02-04
draft: false
tags:
  - Technology
---
- 包含技术：typescript、reactjs、gatsbyjs
- 参考对象：https://github.com/xzhih/hexo-theme-casper

#### 1.前期目标
> 可用性
- [x] 搞出一个能正常读写的博客

#### 2. 中期目标
> 易用性
- [ ] 中英双语
- [ ] PWA
- [ ] 标签云
- [ ] 浏览便利
- [ ] 全局搜索

#### 3. 后期目标
> 易维护性
- [ ] seo
- [ ] 数据统计
- [ ] 集成contentful和neetlify管理内容和版本
